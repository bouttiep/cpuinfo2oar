#!/usr/bin/env python
# coding: utf-8

import subprocess, os, sys

cpuinfo_cmd = "ssh -p 6667 "+sys.argv[1]+" cat /proc/cpuinfo"
oarnodes_cmd = "/usr/bin/oarnodes -Y --sql \"ip='"+sys.argv[1]+"'\"|awk -F\" *: *\" '{if (match($1,\" +core$\")) core=$2; if (match($1,\" +cpu$\")) print $2 \":\" core}'|sort"
oarnodesetting_cmd = "/usr/sbin/oarnodesetting"


def get_cpuinfo():
    """ return useful lists from /pro/cpuinfo file"""
    core_list, proc_list, phys_list = [], [], []
    all_info = subprocess.check_output(cpuinfo_cmd, shell=True).strip()

    # Get processor, physical and core ids from cpuinfo
    for line in all_info.decode().split("\n"):
        if "core id" in line:
            core_list.append(int(line.split(':')[1].replace(' ','')))
        elif "processor" in line:
            proc_list.append(int(line.split(':')[1].replace(' ','')))
        elif "physical id" in line:
            phys_list.append(int(line.split(':')[1].replace(' ','')))

    return phys_list, proc_list, core_list


def get_oarinfo():
    """return cpu numbers from oar for each core"""
    all_info = subprocess.check_output(oarnodes_cmd, shell=True).strip()
    oar_list = []
    for line in all_info.decode().split('\n'):
        oar_list.append(int(line.split(':')[1]))

    return oar_list


def associate_info(oar_list, phys_list, proc_list, core_list):
    """Associate cpu numbers from oar to correct cpu numbers
    from /proc/cpuinfo"""
    cores = list(set(phys_list))
    n_cores = len(cores)
    c_num = []
    for c in cores:
        ind = [i for i in range(len(phys_list)) if phys_list[i] == c]
        n = int(len(oar_list)/n_cores)
        for i in ind[:n]:
            c_num.append(proc_list[i])

    out = dict(zip(oar_list, c_num))
    print(out)
    for o,c in out.items():
        cmd = oarnodesetting_cmd + " -p cpuset=\"" + str(c) + "\" --sql \"core=" + str(o) + "\""
        print subprocess.check_output(cmd, shell=True).strip()

if __name__ == "__main__":
    phys_list, proc_list, core_list = get_cpuinfo()
    associate_info(get_oarinfo(), phys_list, proc_list, core_list)
